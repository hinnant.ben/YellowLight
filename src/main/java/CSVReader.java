import java.io.File;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.List;
import java.util.ArrayList;

public class CSVReader {

	private List<List<String>> rows;
	private static Pattern delim = Pattern.compile("\\s*,\\s*");

	public CSVReader(Scanner line) {
		readFile(line);
	}

	public CSVReader(InputStream is) {
		readFile(new Scanner(is));
	}

	public CSVReader(File file) {
		try {
			readFile(new Scanner(file));
		} catch(FileNotFoundException e) {
			throw new IllegalArgumentException(e.toString());
		}
	}

	public CSVReader(String fname) {
		try {
			readFile(new Scanner(new File(fname)));
		} catch(FileNotFoundException e) {
			throw new IllegalArgumentException(e.toString());
		}
	}

	private void readFile(Scanner line) {
		rows = new ArrayList<List<String>>();
		while(line.hasNext()) {
			List<String> row = new ArrayList<String>();
			Scanner word = new Scanner(line.nextLine()).useDelimiter(delim);
			while(word.hasNext())
				row.add(word.next());
			rows.add(row);
		}
	}

	public int getHeight() {
		return rows.size();
	}

	public int getWidth(int row) {
		return rows.get(row).size();
	}

	public String getString(int col, int row) {
		if(row >= rows.size() || col >=rows.get(row).size())
			return null;
		return rows.get(row).get(col);
	}

	public List<String> getRow(int row) {
		if(row >= rows.size())
			return null;
		return new ArrayList(rows.get(row));
	}

	public int colOf(int row, int col, String target) {
		if(row >= rows.size())
			return -1;
		for(; col < rows.get(row).size(); col++)
			if(rows.get(row).get(col).equals(target))
				return col;
		return -1;
	}

	public int rowOf(int row, int col, String target) {
		for(; row < rows.size(); row++)
			if(colOf(row, col, target) > 0)
				return row;
		return -1;
	}

}
