import org.json.simple.*;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.io.BufferedReader;
import java.io.IOException;

public class ResultPostHandler {

	private String fname;

	public ResultPostHandler(String fname) {
		this.fname = fname;
	}

	public void handle(HttpServletRequest req)
		throws IOException {

		String line = req.getParameter("scenario") + ","
			+ req.getParameter("person") + ","
			+ req.getParameter("trial") + ","
			+ req.getParameter("choice") + ","
			+ req.getParameter("result") + ","
			+ req.getParameter("startT") + ","
			+ req.getParameter("endT") + "\n";

		Files.write(Paths.get(fname), line.getBytes(),
			StandardOpenOption.APPEND);

	}

}
