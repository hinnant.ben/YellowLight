import org.json.simple.*;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ScenarioGetHandler {

	private CSVReader csv;
	private Pattern pattern = Pattern.compile("^/(\\w+)$");

	public ScenarioGetHandler(String fname) {
		this.csv = new CSVReader(fname);
	}

	public String handle(HttpServletRequest req) {

		Matcher matcher = pattern.matcher(req.getPathInfo());
		if(matcher.matches())
			return getOne(matcher.group(1));
		return getAll();

	}

	public String getOne(String id) {
		int name = 0,
			trial = 1,
			greenT = 2,
			yellowT = 3,
			redT = 4,
			leftStimT = 5,
			leftStim = 6,
			rightStimT = 7,
			rightStim = 8,
			crash = 9,
			safeScore = 10,
			crashScore = 11,
			crossCarScore = 12,
			scoreVisibility = 13;


		JSONArray arr = new JSONArray();

		for(int row = 1; row < csv.getHeight(); row++) {
			if(csv.getString(name, row).equals(id)) {
				JSONObject obj = new JSONObject();
				obj.put("trial", Integer.parseInt(
					csv.getString(trial, row)));
				obj.put("greenT", Integer.parseInt(
					csv.getString(greenT, row)));
				obj.put("yellowT", Integer.parseInt(
					csv.getString(yellowT, row)));
				obj.put("redT", Integer.parseInt(
					csv.getString(redT, row)));
				obj.put("leftStimT", Integer.parseInt(
					csv.getString(leftStimT, row)));
				obj.put("leftStim", csv.getString(leftStim, row));
				obj.put("rightStimT", Integer.parseInt(
					csv.getString(rightStimT, row)));
				obj.put("rightStim", csv.getString(rightStim, row));
				obj.put("crash", Boolean.parseBoolean(
					csv.getString(crash, row)));
				obj.put("safeScore", Double.parseDouble(
					csv.getString(safeScore, row)));
				obj.put("crashScore", Double.parseDouble(
					csv.getString(crashScore, row)));
				obj.put("crossCarScore", Double.parseDouble(
					csv.getString(crossCarScore, row)));
				obj.put("scoreVisibility", Boolean.parseBoolean(
					csv.getString(scoreVisibility, row)));
				arr.add(obj);
			}
		}
		return arr.toString();
	}

	public String getAll() {
		JSONArray arr = new JSONArray();
		int name = 0;
		for(int row = 1; row < csv.getHeight(); row++)
			if(!arr.contains(csv.getString(name, row)))
				arr.add(csv.getString(name, row));
		return arr.toString();
	}

}
