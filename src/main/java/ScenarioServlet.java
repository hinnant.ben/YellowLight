import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.PrintWriter;
import java.io.IOException;

public class ScenarioServlet extends HttpServlet {

	private ScenarioGetHandler handler;

	@Override
	public void init(ServletConfig config) {
		handler = new ScenarioGetHandler(config.getInitParameter("scenarios")
			.replace("~", System.getProperty("user.home")));
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		resp.setContentType("application/json");
		PrintWriter out = resp.getWriter();
		try {
			out.print(handler.handle(req));
		} catch(Exception e) {
			throw new ServletException(e.getMessage());
		}

	}

}
