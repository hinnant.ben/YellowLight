import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.PrintWriter;
import java.io.IOException;

public class ResultServlet extends HttpServlet {

	private ResultPostHandler handler;

	@Override
	public void init(ServletConfig config) {
		handler = new ResultPostHandler(config.getInitParameter("results")
			.replace("~", System.getProperty("user.home")));
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		try {
			handler.handle(req);
		} catch(Exception e) {
			throw new ServletException(e.getMessage());
		}

	}

}
