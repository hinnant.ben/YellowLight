import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.util.regex.*;

public class ImageServlet extends HttpServlet {

	private File root;
	private static Pattern p = Pattern.compile("^/(\\w+)$");
	private ServletContext context;

	@Override
	public void init(ServletConfig config) {
		root = new File(config.getInitParameter("images")
			.replace("~", System.getProperty("user.home")));
		context = config.getServletContext();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		if(req.getPathInfo() == null) {
			resp.sendError(404);
			return;
		}
		Matcher match = p.matcher(req.getPathInfo());
		String fname = null;
		if(match.matches())
			fname = findFile(match.group(1));
		if(fname == null) {
			resp.sendError(404);
			return;
		}

		File file = new File(root + "/" + fname);
		resp.setContentType(context.getMimeType(fname));
		resp.setContentLength((int)file.length());

		FileInputStream in = new FileInputStream(file);
		OutputStream out = resp.getOutputStream();

		byte[] buf = new byte[1024];
		int bytes = 0;
		while((bytes = in.read(buf)) >= 0)
			out.write(buf, 0, bytes);
		out.close();
		in.close();

	}

	private String findFile(String fname) {
		for(String file : root.list())
			if(file.startsWith(fname))
				return file;
		return null;
	}

}
