(function() {

	var cache = {};
	var waiting = {};

	function register(module) {
		if(typeof module === 'undefined')
			throw 'module undefined';
		if(typeof module.name !== 'string')
			throw 'module name not string: ' + module.name;
		console.log('registering module: ' + module.name
			+ ' depends: ' + module.deps);
		var i = ready(module);
		if(i < 0) {
			if(typeof module.init === 'function')
				module.init(dependencies(module));
			cache[module.name] = module;
			resume(module.name);
		} else
			wait(module.deps[i], module);
	}

	function ready(module) {
		if(typeof module.deps === 'undefined')
			module.deps = [];
		if(!Array.isArray(module.deps))
			throw 'dependencies not array: ' + module.deps;
		for(var i = 0; i < module.deps.length; i++)
			if(!(module.deps[i] in cache))
				return i;
		return -1;
	}

	function wait(dependency, module) {
		if(!(dependency in waiting))
			waiting[dependency] = [];
		waiting[dependency].push(module);
	}

	function resume(dependency) {
		if(!(dependency in waiting))
			return;
		while(waiting[dependency].length > 0)
			register(waiting[dependency].pop());
		delete waiting[dependency];
	}

	function dependencies(module) {
		var out = {};
		for(var i = 0; i < module.deps.length; i++)
			out[module.deps[i]] = cache[module.deps[i]];
		return out;
	}

	function fetchManifest(path) {
		var xhttp = new XMLHttpRequest();
		xhttp.open('GET', path, true);
		xhttp.onreadystatechange = function() {
			if(this.readyState == XMLHttpRequest.DONE)
				fetchModules(JSON.parse(xhttp.responseText));
		};
		xhttp.send();
	}

	function fetchModules(manifest) {
		if(!Array.isArray(manifest))
			throw 'manifest not array: ' + manifest;
		for(var i = 0; i < manifest.length; i++)
			fetchModule(manifest[i]);
		if(waiting.length > 0)
			console.err('missing dependencies: ' + waiting);
	}

	function fetchModule(path) {
		if(typeof path !== 'string')
			throw 'path not string: ' + path;
		var xhttp = new XMLHttpRequest();
		xhttp.open('GET', path, true);
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4)
				evaluateModule(xhttp.responseText);
		};
		xhttp.send();
	}

	function evaluateModule(module) {
		new Function('register', module)(register);
	}

	fetchManifest(document.currentScript.dataset.manifest);

})();
