var interval = 1000 / 60;

var layers = [];
var canvas, context;
var then, now;

function init() {
	// FIX THIS HARDCODED CANVAS ID
	canvas = document.getElementById('screen');
	context = canvas.getContext('2d');
	resize();
	var then = performance.now();
	var now = then;
	requestAnimationFrame(drawLayers);
}

function resize() {
	canvas.width = 769;
	canvas.height = 576;
}

function floor(key) {
	for(var index = 0; index < layers.length; index++)
		if(key >= layers[index].z)
			return index;
	return layers.length;
}

function find(name) {
	for(var index = 0; index < layers.length; index++)
		if(name == layers[index].name)
			return index;
	return -1;
}

function addLayer(layer) {
	if(typeof layer.z !== 'number')
		throw 'layer z-level not present or wrong type';
	if(typeof layer.draw !== 'function')
		throw 'layer draw function not present';
	layers.splice(floor(layer.z), 0, layer);
}

function removeLayer(name) {
	var index = find(name);
	if(index < 0)
		throw new Error('layer does not exist: ' + name);
	layers.splice(index, 1);
}

function drawLayers() {
	now = performance.now();
	var delta = now - then;
	then = now;
	for(var index = 0; index < layers.length; index++) {
		try {
			var frame = layers[index].draw (
				canvas.width,
				canvas.height,
				delta
			);
			context.drawImage (
				frame.canvas,
				frame.x,
				frame.y,
				frame.w,
				frame.h
			);
		} catch(error) {
			console.error('draw error: ' + error);
		}
	}
	requestAnimationFrame(drawLayers);
}

register ({
	name : 'Compositor',
	addLayer : addLayer,
	removeLayer : removeLayer,
	init : init
});
