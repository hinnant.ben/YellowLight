function Set() {
	this.arr = [];
}

Set.prototype = {
	has : has,
	add : add,
	remove : remove,
	forEach : forEach
};

function has(val) {
	return this.arr.indexOf(val) >= 0;
}

function add(val) {
	if(this.has(val))
		return false;
	this.arr.push(val);
	return true;
}

function remove(val) {
	var i = this.arr.indexOf(val);
	if(i < 0)
		return false;
	this.arr.splice(i, 1);
	return true;
}

function forEach(func) {
	this.arr.forEach(func);
}

register ({
	Set : Set,
	name : 'Set'
});
