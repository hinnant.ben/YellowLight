var handlers = {};
var dQ = [];
var Set;

function addHandler(eventIn, handlerIn) {
	if(typeof handlerIn !== 'function')
		throw 'handler not function';
	if(typeof eventIn !== 'string')
		throw 'event name not string';
	if(!(eventIn in handlers))
		handlers[eventIn] = new Set();
	if(!handlers[eventIn].add(handlerIn))
		throw 'duplicate handler: ' + eventIn;
}

function removeHandler(eventIn, handlerIn) {
	if(typeof handlerIn !== 'function')
		throw 'handler not function';
	if(typeof eventIn !== 'string')
		throw 'event name not string';
	if(!(eventIn in handlers))
		throw 'event name not found';
	if(typeof handlers[eventIn].remove(handlerIn) === 'undefined')
		throw 'handler not found: ' + eventIn;
}

function pushEvent(eventIn, argumentsIn) {
	if(typeof eventIn !== 'string')
		throw 'event name not string';
	if(typeof argumentsIn === 'undefined')
		argumentsIn = [];
	dQ.push ({
		name : eventIn,
		arguments : argumentsIn
	});
}

function enqueEvent(eventIn, argumentsIn) {
	if(typeof eventIn !== 'string')
		throw 'event name not string';
	if(typeof argumentsIn === 'undefined')
		argumentsIn = [];
	dQ.unshift ({
		name : eventIn,
		arguments : argumentsIn
	});
}

function pollEvent() {
	while(dQ.length > 0)
		dispatchEvent(dQ.pop());
}

function dispatchEvent(eventIn) {
	var name = 'on' + eventIn.name;
	if(name in handlers) {
		handlers[name].forEach (
			function(handler) {
				try {
					handler.apply(handler, eventIn.arguments);
				} catch(error){
					console.error(eventIn.name + ': ' + error);
				}
			}
		);
	}
}

function init(deps) {
	Set = deps.Set.Set;
	// NOTE: this seems bad, but will be fine for now
	setInterval(pollEvent, 0);
}

register ({
	name : 'Mediator',
	addHandler : addHandler,
	removeHandler : removeHandler,
	pushEvent : pushEvent,
	enqueEvent : enqueEvent,
	deps : ['Set'],
	init : init
});
