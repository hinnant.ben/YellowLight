var obj = {};

function init() {
	var params = window.location.search.substr(1).split('&');
	if(params[0] == '')
		params = [];

	for(var i = 0; i < params.length; i++) {
		var keyval = params[i].split('=');
		obj[keyval[0]] = keyval[1];
	}
}

function get(key) {
	return obj[key];
}

register({
	name : 'Get',
	get : get,
	init : init
});
