var Compositor, Mediator, Data;

var canvas, context, resultT, endT, x, destX, truckImg = new Image();
var textCanvas, textContext;

function render(width, height) {
//	resize(width, height);
	context.drawImage(truckImg, 50, 300);
}

function renderText(text) {
	textContext.clearRect(0, 0, textCanvas.width, textCanvas.height);
	textContext.fillStyle = text == 'crash' ? 'red' : 'darkgreen';
	var width = textCanvas.width / text.length;
	textContext.font = width + 'px Georgia';
	textContext.fillText (
		text,
		(textCanvas.width / 2) - (width / 2),
		textCanvas.height / 2
	);
}

function drawText(height, width, delta) {
	return {
		canvas : textCanvas,
		x : 0,
		y : 0,
		w : textCanvas.width,
		h : textCanvas.height
	};
}

function drawStill(width, height, delta) {
	render(width, height);
	return {
		canvas : canvas,
		x : destX,
		y : 0,
		w : canvas.width,
		h : canvas.height
	};
}

function draw(width, height, delta) {
	render(width, height);
	x += pixelsPerMillisecond(performance.now()) * delta;
	return {
		canvas : canvas,
		x : x,
		y : 0,
		w : canvas.width,
		h : canvas.height
	};
}

function pixelsPerMillisecond(nowT) {
	var remainingX = destX - x;
	var remainingT = endT - nowT;
	return remainingX / remainingT;
}

function resize(width, height) {
	canvas.width = width;
	canvas.height = height;
}

function onResultOff() {
	Compositor.removeLayer('truck');
	Compositor.removeLayer('text');
	Mediator.removeHandler('onResultOff', onResultOff);
}

function init(deps) {
	canvas = document.createElement('canvas');
	canvas.width = 768;
	canvas.height = 576;
	context = canvas.getContext('2d');
	textCanvas = document.createElement('canvas');
	textCanvas.width = 768;
	textCanvas.height = 576;
	textContext = textCanvas.getContext('2d');
	truckImg.src = 'resources/truck.png';
	Compositor = deps.Compositor;
	Mediator = deps.Mediator;
	Data = deps.Data;
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

function onScenarioStart() {
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
	Mediator.addHandler('onResultOn', onResultOn);
}

function onScenarioStop() {
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onResultOn', onResultOn);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
}

function onIntersectionStart(index) {
	resultT = Data.getResultT(index - 1);
}

function onResultOn(result) {
	Mediator.addHandler('onResultOff', onResultOff);
	endT = resultT / 2 + performance.now();
	truckStart(result);
}

function textStart(result) {
	renderText(result);
	Compositor.addLayer({draw : drawText, z : 0, name : 'text'});
}

function truckStop(result) {
	Mediator.pushEvent('TruckStop', [result]);
}

function truckStart(result) {
	x = canvas.width * -1;
	destX = 0;
	switch(result) {
		case 'crash':
			destX = 0;
			break;
		case 'crosscar':
			destX = canvas.width;
			break;
		case 'safe':
			destX = canvas.width * -1;
			break;
		default:
			throw 'invalid result state';
	}
	Compositor.addLayer({draw : draw, z : 0, name : 'truck'});
	Mediator.addHandler('onTruckStop', onTruckStop);
	setTimeout(truckStop, resultT / 2, result);
}

function onTruckStop(result) {
	Compositor.removeLayer('truck');
	Compositor.addLayer({draw : drawStill, z : 0, name : 'truck'});
	textStart(result);
	Mediator.removeHandler('onTruckStop', onTruckStop);
}

register ({
	name : 'crashGraphics',
	deps : ['Compositor', 'Mediator', 'Data'],
	init : init
});
