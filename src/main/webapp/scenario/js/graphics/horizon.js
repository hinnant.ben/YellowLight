var Mediator, Data;
var canvas = document.createElement('canvas');
var context = canvas.getContext('2d');
var roadY = 0, roadH = 0;
var timeEnd = 0;

function draw(width, height, delta) {
	render(width, height, delta);
	return {canvas : canvas, x : 0, y : 0, w : width, h : height};
}

function drawResult(width, height, delta) {
	context.clearRect(0, 0, width, height);
	context.fillStyle = 'black';
	context.fillRect(0, 400, width, 176)
	return {canvas : canvas, x : 0, y : 0, w : width, h : height};
}

function render(width, height, delta) {
	context.clearRect(0, 0, width, height);
	var velocity = pixelsPerMillisecond(height, performance.now());
	roadH += velocity.H * delta;
	roadY += velocity.Y * delta;
	context.fillStyle = 'black';
	context.fillRect(0, roadY, width, roadH);
}

function init(deps) {
	canvas.width = 768;
	canvas.height = 576;
	Mediator = deps.Mediator;
	Data = deps.Data;
	Compositor = deps.Compositor;
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

function onScenarioStart() {
	// only listen for further events if data is accessible
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
	Mediator.addHandler('onResultOn', onResultOn);
	Mediator.addHandler('onResultOff', onResultOff);
}

function onScenarioStop() {
	// stop listening for events until next scenario
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
	Mediator.removeHandler('onResultOn', onResultOn);
	Mediator.removeHandler('onResultOff', onResultOff);
}

function onResultOn() {
	Compositor.addLayer({draw : drawResult, z : 4, name : 'horizon'});
}

function onResultOff() {
	Compositor.removeLayer('horizon');
}

function pixelsPerMillisecond(height, timeNow) {
	var remainingH = 176 - roadH;
	var remainingY = 400 - roadY;
	var remainingT = timeEnd - timeNow;
	return {H : remainingH / remainingT, Y : remainingY / remainingT};
}

function onIntersectionStart(index) {
	roadH = 1;
	roadY = 1;
	timeEnd = performance.now() + Data.getLightT(index - 1);
	Compositor.addLayer({draw : draw, z : 4, name : 'horizon'});
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
}

function onIntersectionStop() {
	Compositor.removeLayer('horizon');
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
}

register ({
	name : 'graphicsHorizon',
	deps : ['Compositor', 'Mediator', 'Data'],
	init : init
});
