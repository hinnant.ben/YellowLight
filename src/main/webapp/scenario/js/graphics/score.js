var Compositor, Mediator, Data;
var score = 0.0;
var canvas, context;

function onIntersectionStart(index) {
	if(Data.getResultVisibility(index - 1))
		Compositor.addLayer({draw : draw, z : 0, name : 'scoreboard'});
	Mediator.addHandler('onScore', onScore);
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
}

function onIntersectionStop() {
	Compositor.removeLayer('scoreboard');
	Mediator.removeHandler('onScore', onScore);
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
}

function onScore(scoreIn) {
	score += scoreIn;
}

function draw(width, height, delta) {
	canvas.width = width * 0.25;
	canvas.height = height * 0.07;
	context.fillStyle = 'grey';
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = 'green';
	context.font = (canvas.width / 5) + 'px Georgia';
	context.fillText('$' + score.toFixed(2), 2, canvas.width / 5);
	return {
		canvas : canvas,
		x : (width / 2) - (canvas.width / 2),
		y : 5,
		w : canvas.width,
		h : canvas.height
	};
}

function init(deps) {
	canvas = document.createElement('canvas');
	context = canvas.getContext('2d');

	Compositor = deps.Compositor;
	Mediator = deps.Mediator;
	Data = deps.Data;
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
}

register ({
	name : 'graphicsScore',
	deps : ['Compositor', 'Mediator', 'Data'],
	init : init
});
