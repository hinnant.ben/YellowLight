var Data, Compositor, Mediator;
var limg, rimg;

function onScenarioStart() {
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
	Mediator.addHandler('onStimOn', onStimOn);
	Mediator.addHandler('onStimOff', onStimOff);
}

function onScenarioStop() {
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
	Mediator.removeHandler('onStimOn', onStimOn);
	Mediator.removeHandler('onStimOff', onStimOff);
}

function onStimOn(stim) {
	switch(stim) {
		case 'left':
			Compositor.addLayer({draw : ldraw, z : 0, name : 'lstim'});
			break;
		case 'right':
			Compositor.addLayer({draw : rdraw, z : 0, name : 'rstim'});
			break;
		default:
			throw 'invalid stim: ' + stim;
	}
}

function onStimOff(stim) {
	switch(stim) {
		case 'left':
			Compositor.removeLayer('lstim');
			break;
		case 'right':
			Compositor.removeLayer('rstim');
			break;
		default:
			throw 'invalid stim: ' + stim;
	}
}

function onIntersectionStart(index) {
	limg = new Image();
	var stim = Data.getLeftStim(index - 1);
	if(typeof stim !== 'string' || stim == ''
		|| stim.toLowerCase() == 'none')
			return;
	limg.src = '../app/stim/' + stim;

	rimg = new Image();
	stim = Data.getLeftStim(index - 1);
	if(typeof stim !== 'string' || stim == ''
		|| stim.toLowerCase() == 'none')
			return;
	rimg.src = '../app/stim/' + stim;
}

function onIntersectionStop() {
	limg.src = '';
	rimg.src = '';
}

function ldraw(width, height, delta) {
	return {
		canvas : limg,
		x : width - width * 0.25 - 5,
		y : 5,
		w : width * 0.25,
		h : limg.height * ((width * 0.25) / limg.width)
	};
}

function rdraw(width, height, delta) {
	return {
		canvas : rimg,
		x : 5,
		y : 5,
		w : width * 0.25,
		h : rimg.height * ((width * 0.25) / rimg.width)
	};
}

function init(deps) {
	Data = deps.Data;
	Compositor = deps.Compositor;
	Mediator = deps.Mediator;
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

register ({
	name : 'graphicsStim',
	deps : ['Compositor', 'Data', 'Mediator'],
	init : init
});
