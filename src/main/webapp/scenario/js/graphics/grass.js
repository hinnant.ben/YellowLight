var canvas, context;

function init(deps) {
	canvas = document.createElement('canvas');
	context = canvas.getContext('2d');
	resize(576, 768);
	deps.Compositor.addLayer({draw : draw, z : 5, name : 'grass'});
}

function draw(height, width, delta) {
	return {
		canvas : canvas,
		x : 0,
		y : canvas.height,
		w : canvas.width,
		h : canvas.height
	};
}

function render(height, width) {
	context.fillStyle = 'green';
	context.fillRect(0, 0, width, height);
}

function resize(height, width) {
	canvas.width = width;
	canvas.height = height / 2;
	render(canvas.height, canvas.width);
}

register ({
	name : 'graphicsGrass',
	deps : ['Compositor'],
	init : init
});
