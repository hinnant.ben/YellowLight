var canvas = document.createElement('canvas');
var context = canvas.getContext('2d');

function render(width, height) {
	canvas.width = width;
	canvas.height = height / 2;
	context.fillStyle = 'black';
	context.beginPath();
	context.moveTo(canvas.width / 2, 0);
	context.lineTo(0, canvas.height);
	context.lineTo(canvas.width, canvas.height);
	context.fill();
}

function draw(width, height, delta) {
	if(width / 2 != canvas.width || height / 2 != canvas.height)
		render(width, height);
	return {
		canvas : canvas,
		x : 0,
		y : canvas.height,
		w : canvas.width,
		h : canvas.height
	};
}

function init(deps) {
	deps.Compositor.addLayer({draw : draw, z : 1, name : 'road'});
}

register ({
	name : 'graphicsRoad',
	deps : ['Compositor'],
	init : init
});
