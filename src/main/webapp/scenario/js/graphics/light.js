var Compositor, Mediator, Data;

var canvas, context;

var scaleWidth = 1, scaleHeight = 1;
var redImg = new Image(), greenImg = new Image(), yellowImg = new Image();
var endT = 0;
var color;

function render(width, height, delta) {
	resize(width, height);
	var velocity = pixelsPerMillisecond(width, height, performance.now());
	scaleWidth += velocity.x * delta;
	scaleHeight += velocity.y * delta;
	renderLight(width, height, delta);
}

function draw(width, height, delta) {
	render(width, height, delta);
	return {
		canvas : canvas,
		x : (width / 2) - (scaleWidth / 2),
		y : (height / 2) - (scaleHeight / 2),
		w : scaleWidth,
		h : scaleHeight
	};
}

function drawResult(width, height, delta) {
	return {
		canvas : canvas,
		x : 0,
		y : 0,
		w : width,
		h : height
	};
}

function resize(width, height) {
	canvas.width = width;
	canvas.height = height;
}

function renderLight(width, height) {
	var img;
	switch(color) {
		case 'green':
			img = greenImg;
			break;
		case 'yellow':
			img = yellowImg;
			break;
		case 'red':
			img = redImg;
			break;
		default:
			throw 'invalid light color: ' + color;
	}
	context.drawImage(img, 150, 50);
}

function pixelsPerMillisecond(width, height, nowT) {
	var remainingX = width - scaleWidth;
	var remainingY = height - scaleHeight;
	var remainingT = endT - nowT;
	var xVelocity = remainingX / remainingT;
	var yVelocity = remainingY / remainingT;
	return {x : xVelocity, y : yVelocity};
}

function onScenarioStart() {
	// only listen for further events if data is accessible
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
	Mediator.addHandler('onLightOn', onLightOn);
	Mediator.addHandler('onResultOn', onResultOn);
	Mediator.addHandler('onResultOff', onResultOff);
}

function onScenarioStop() {
	// stop listening for events until next scenario
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
	Mediator.removeHandler('onLightOn', onLightOn);
	Mediator.removeHandler('onResultOn', onResultOn);
	Mediator.removeHandler('onResultOff', onResultOff);
}

function onIntersectionStart(index) {
	scaleWidth = 1;
	scaleHeight = 1;
	endT = performance.now() + Data.getLightT(index - 1);
	Compositor.addLayer({draw : draw, z : 0, name : 'light'});
}

function onIntersectionStop() {
	Compositor.removeLayer('light');
}

function onLightOn(colorIn) {
	color = colorIn;
}

function onResultOn(result) {
	Compositor.addLayer({draw : drawResult, z : 0, name : 'light'});
}

function onResultOff() {
	Compositor.removeLayer('light');
}

function init(deps) {
	Mediator = deps.Mediator;
	Data = deps.Data;
	Compositor = deps.Compositor;
	greenImg.src = 'resources/green.png';
	yellowImg.src = 'resources/yellow.png';
	redImg.src = 'resources/red.png';
	Mediator.addHandler('onScenarioStart', onScenarioStart);
	canvas = document.createElement('canvas');
	context = canvas.getContext('2d');
}

register ({
	name : 'lightGraphics',
	deps : ['Mediator', 'Data', 'Compositor'],
	init : init
});
