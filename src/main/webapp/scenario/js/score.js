var Mediator, Data;

var stop = false;
var index = 0;

function onScenarioStart() {
	Mediator.addHandler('onPlayerChoice', onPlayerChoice);
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
}

function onScenarioStop() {
	Mediator.removeHandler('onPlayerChoice', onPlayerChoice);
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
}

function onIntersectionStart(indexIn) {
	index = indexIn;
}

function onPlayerChoice(choice) {
	stop = choice == 'stop';
}

function onIntersectionStop() {
	Mediator.pushEvent('Score', [result()]);
}

function init(deps) {
	Mediator = deps.Mediator;
	Data = deps.Data;
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

register ({
	name : 'score',
	deps : ['Data', 'Mediator'],
	init : init
});

function noRed() {
	return Data.getRedT(index - 1) == 0;
}

function isCrash() {
	return Data.getCrash(index - 1);
}

function stopped() {
	return stop;
}

function result() {
	return noRed() || !isCrash() ? Data.getSafeScore(index - 1)
		: !stopped() ? Data.getCrashScore(index - 1)
		: Data.getCrossCarScore(index - 1);
}
