var Mediator, Data;

var startT, endT, choice, index;

function onScenarioStart() {
	Mediator.addHandler('onPlayerChoice', onPlayerChoice);
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onResultOn', onResultOn);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
}

function onScenarioStop() {
	Mediator.removeHandler('onPlayerChoice', onPlayerChoice);
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onResultOn', onResultOn);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
}

function onResultOn(result) {
	endT = Date.now();
	var body = 'scenario=' + Data.getScenario()
		+ '&person=' + Data.getPerson()
		+ '&trial=' + index
		+ '&choice=' + choice
		+ '&result=' + result
		+ '&startT=' + startT
		+ '&endT=' + endT;
	var xhttp = new XMLHttpRequest();
	xhttp.open('POST', '../app/result', true);
	xhttp.setRequestHeader('Content-type',
		'application/x-www-form-urlencoded');
	xhttp.onreadystatechange = function() {
		if(this.readyState == XMLHttpRequest.DONE && this.status != 200)
			console.err('POST status: ' + this.status + ' body: ' + body);
	};
	xhttp.send(body);
}

function init(deps) {
	Mediator = deps.Mediator;
	Data = deps.Data;
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

function onPlayerChoice(choiceIn) {
	choice = choiceIn;
}

function onIntersectionStart(indexIn) {
	choice = undefined;
	index = indexIn;
	startT = Date.now();
}

register ({
	name : 'record',
	deps : ['Data', 'Mediator'],
	init : init
});
