var scenario = "";
var person = "";
var data = [];

function setScenario(scenarioIn) {
	scenario = scenarioIn;
}

function setPerson(personIn) {
	person = personIn;
}

function setData(dataIn) {
	data = dataIn;
}

function getGreenT(index) {
	return data[index].greenT;
}

function getYellowT(index) {
	return data[index].yellowT;
}

function getRedT(index) {
	return data[index].redT;
}

function getRightStimT(index) {
	return data[index].rightStimT;
}

function getRightStim(index) {
	return data[index].rightStim;
}

function getLeftStimT(index) {
	return data[index].leftStimT;
}

function getLeftStim(index) {
	return data[index].leftStim;
}

function getCrash(index) {
	return data[index].crash;
}

function getSafeScore(index) {
	return data[index].safeScore;
}

function getCrashScore(index) {
	return data[index].crashScore;
}

function getCrossCarScore(index) {
	return data[index].crossCarScore;
}

function getResultVisibility(index) {
	return data[index].scoreVisibility;
}

function getLightT(index) {
	return getRedT(index) + getYellowT(index) + getGreenT(index);
}

function getScenario() {
	return scenario;
}

function getPerson() {
	return person;
}

function getLength() {
	return data.length;
}

register ({
	name : 'Data',
	setPerson : setPerson,
	setScenario : setScenario,
	setData : setData,
	getGreenT : getGreenT,
	getYellowT : getYellowT,
	getRedT : getRedT,
	getRightStimT : getRightStimT,
	getRightStim : getRightStim,
	getLeftStimT : getLeftStimT,
	getLeftStim : getLeftStim,
	getCrash : getCrash,
	getSafeScore : getSafeScore,
	getCrashScore : getCrashScore,
	getCrossCarScore : getCrossCarScore,
	getScenario : getScenario,
	getPerson : getPerson,
	getLength : getLength,
	getLightT : getLightT,
	getResultVisibility : getResultVisibility,
	getResultT : function(){return 5200;}
});
