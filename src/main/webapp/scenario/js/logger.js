function init(deps) {
	deps.Mediator.addHandler (
		'onLightOn',
		function(color) {
			console.log('light on:  ' + color);
		}
	);
	deps.Mediator.addHandler (
		'onLightOff',
		function(color) {
			console.log('light off:  ' + color);
		}
	);
	deps.Mediator.addHandler (
		'onIntersectionStart',
		function(index) {
			console.log('starting intersection: ' + index);
		}
	);
	deps.Mediator.addHandler (
		'onIntersectionStop',
		function(index) {
			console.log('stopping intersection: ' + index);
		}
	);
	deps.Mediator.addHandler (
		'onScenarioStart',
		function() {
			console.log('starting scenario');
		}
	);
	deps.Mediator.addHandler (
		'onScenarioStop',
		function() {
			console.log('stopping scenario');
		}
	);
	deps.Mediator.addHandler (
		'onResultOn',
		function(result) {
			console.log('result on: ' + result);
		}
	);
	deps.Mediator.addHandler (
		'onResultOff',
		function(result) {
			console.log('result off: ' + result);
		}
	);
	deps.Mediator.addHandler (
		'onPlayerChoice',
		function(choice) {
			console.log('player choice: ' + choice);
		}
	);
	deps.Mediator.addHandler (
		'onStimOn',
		function(stim) {
			console.log('stim on: ' + stim);
		}
	);
	deps.Mediator.addHandler (
		'onStimOff',
		function(stim) {
			console.log('stim off: ' + stim);
		}
	);
	deps.Mediator.addHandler (
		'onScore',
		function(score) {
			console.log(score + ' points!');
		}
	);
}

register ({
	name : 'Logger',
	deps : ['Mediator'],
	init : init
});
