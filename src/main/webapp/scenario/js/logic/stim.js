var Data, Mediator;
var leftTimeout = 0, rightTimeout = 0;

function onScenarioStart() {
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
}

function onScenarioStop() {
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
}

function onIntersectionStart(index) {
	if(leftTimeout > 0 || rightTimeout > 0)
		console.error('stim timeout already set: left: '
			+ leftTimeout + ' right: ' + rightTimeout);
	if(Data.getRightStimT(index - 1) > 0)
		rightTimeout = setTimeout(stimOn,
			Data.getRightStimT(index - 1), 'right');
	if(Data.getLeftStimT(index - 1) > 0)
		leftTimeout = setTimeout(stimOn,
			Data.getLeftStimT(index - 1), 'left');
}

function onIntersectionStop() {
	stimOff('left', leftTimeout);
	leftTimeout = 0;
	stimOff('right', rightTimeout);
	rightTimeout = 0;
}

function stimOn(side) {
	Mediator.pushEvent('StimOn', [side]);
}

function stimOff(side, timeout) {
	if(timeout == 0)
		console.error(side + ' stim timeout already cleared');
	clearTimeout(timeout);
	Mediator.pushEvent('StimOff', [side]);
}

function init(deps) {
	Data = deps.Data;
	Mediator = deps.Mediator;
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

register ({
	name : 'logicStim',
	deps : ['Data', 'Mediator'],
	init : init
});
