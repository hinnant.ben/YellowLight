var Mediator;

function left(e) {
	switch(keyIn.button) {
		case 0:
			choice = 'go';
			break;
		case 2:
			choice = 'stop';
			break;
		default:
			throw 'invalid key';
	}
	Mediator.pushEvent('PlayerChoice', [choice]);
}

function right(e) {
	e.preventDefault();
	Mediator.pushEvent('PlayerChoice', ['stop']);
}

function onLightOn(colorIn) {
	if(colorIn == 'yellow') {
		document.addEventListener('click', left);
		document.addEventListener('contextmenu', right);
	}
}

function onLightOff(colorIn) {
	if(colorIn == 'yellow') {
		document.removeEventListener('click', left);
		document.removeEventListener('contextmenu', right);
	}
}

function onPlayerChoice() {
	document.removeEventListener('click', left);
	document.removeEventListener('contextmenu', right);
}

function init(deps) {
	Mediator = deps.Mediator;
	Mediator.addHandler('onLightOn', onLightOn);
	Mediator.addHandler('onLightOff', onLightOff);
	Mediator.addHandler('onPlayreChoie', onPlayerChoice);
}

register ({
	name : 'choiceLogic',
	deps : ['Mediator'],
	init : init
});
