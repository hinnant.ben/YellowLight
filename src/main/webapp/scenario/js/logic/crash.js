// references to the Mediator and DataSet modules
var Mediator, Data;

// index of current intersection
var crash = false, stop = false, red = false, resultT = 0;

function onScenarioStart() {
	// only listen for further events if data is accessible
	Mediator.addHandler('onPlayerChoice', onPlayerChoice);
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
}

function onScenarioStop() {
	// stop listening for events until next scenario
	Mediator.removeHandler('onPlayerChoice', onPlayerChoice);
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
}

function onIntersectionStart(index) {
	// save the index of this intersection for later
	stop = false;
	crash = Data.getCrash(index - 1);
	red = Data.getRedT(index - 1) != 0;
	resultT = Data.getResultT(index - 1);
}

function onPlayerChoice(choice) {
	stop = choice == 'stop';
}

function onIntersectionStop() {
	resultOn();
}

function resultOn() {
	Mediator.pushEvent('ResultOn', [result()]);
	setTimeout(resultOff, resultT);
}

function resultOff() {
	Mediator.pushEvent('ResultOff', [result()]);
}

function result() {
	return !red || !crash ? 'safe'
		: !stop ? 'crash'
		: 'crosscar';
}

function init(deps) {
	// the Mediator and DataSets modules are needed throughout this module
	Mediator = deps.Mediator;
	Data = deps.Data;
	
	// listen for ScenarioStart events
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

// register this module and declare its name and dependencies
register ({
	name : 'crashLogic',
	deps : ['Data', 'Mediator'],
	init : init
});
