// references to the Mediator and DataSet modules
var Mediator, Data;

var colors = ['green', 'yellow', 'red'], times = [0, 0, 0], color = 0;

// when a light is on, this timeout will have a non-zero value
// this is not the amount of time the light will be on;
// it's an ID number for a waiting subroutine that will turn the light off.
var timeout = 0;

function onScenarioStart() {
	// only listen for further events if data is accessible
	Mediator.addHandler('onIntersectionStart', onIntersectionStart);
	Mediator.addHandler('onScenarioStop', onScenarioStop);
}

function onScenarioStop() {
	// stop listening for events until next scenario
	Mediator.removeHandler('onIntersectionStart', onIntersectionStart);
	Mediator.removeHandler('onScenarioStop', onScenarioStop);
}

function onIntersectionStart(index) {
	Mediator.addHandler('onIntersectionStop', onIntersectionStop);
	Mediator.addHandler('onLightOff', lightOn);
	// get the light times for this intersection
	times[0] = Data.getGreenT(index - 1);
	times[1] = Data.getYellowT(index - 1);
	times[2] = Data.getRedT(index - 1);
	color = 0;

	// turn the first light on
	lightOn();
}

function onIntersectionStop() {
	Mediator.removeHandler('onIntersectionStop', onIntersectionStop);
	Mediator.removeHandler('onLightOff', lightOn);
	// clear the timeout if it's still waiting
	if(timeout != 0) {
		clearTimeout(timeout);
		lightOff();
		throw 'light timeout pre-empted';
	}
}

function lightOn() {
	// another light color should not be on right now
	if(timeout != 0) {
		throw 'light timeout not cleared';
	// skip to the next light if this one has a time of 0ms
	} if(color >= times.length) {
		onIntersectionStop();
		throw 'light timeout ceding';
	} else if(times[color] == 0) {
		color++;
		lightOn();
	// turn this light on and set a timer for it to turn off
	} else {
		Mediator.pushEvent('LightOn', [colors[color]]);
		timeout = setTimeout(lightOff, times[color]);
	}
}

function lightOff() {
	// a light should be on before trying to turn it off
	if(timeout == 0)
		throw 'light timeout not set';
	// turn the light off
	Mediator.pushEvent('LightOff', [colors[color++]]);
	timeout = 0;
}

function init(deps) {
	// the Mediator and DataSets modules are needed throughout this module
	Mediator = deps.Mediator;
	Data = deps.Data;
	// listen for ScenarioStart events
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

// register this module and declare its name and dependencies
register ({
	name : 'lightLogic',
	deps : ['Data', 'Mediator'],
	init : init
});
