var Data, Mediator;

// current intersection and number of intersections
var count = 0, index = 1, timeout = 0;

function onScenarioStart() {
	// get number of intersections
	count = Data.getLength();

	// only listenfor further events if data is acessible
	Mediator.addHandler('onScenarioStop', onScenarioStart);
	Mediator.addHandler('onResultOff', intersectionStart);
	intersectionStart();
}

function onScenarioStop() {
	// restart index to 0 until next scenario
	index = 1;
	// stop listening for events until next scenario
	Mediator.removeHandler('onScenarioStop', onScenarioStart);
	Mediator.addHandler('onResultOff', intersectionStart);
}

function intersectionStart() {
	// stop if this is the last intersection
	if(index > count)
		Mediator.pushEvent('ScenarioStop');
	// or broadcast that the next intersection started
	else {
		Mediator.pushEvent('IntersectionStart', [index]);
		timeout = setTimeout(intersectionStop, Data.getLightT(index - 1));
	}
}

function intersectionStop() {
	if(timeout == 0)
		throw 'cannot stop intersection that has not started';
	Mediator.pushEvent('IntersectionStop', [index++]);
	timeout = 0;
}

function init(deps) {
	Data = deps.Data;
	Mediator = deps.Mediator;
	Mediator.addHandler('onScenarioStart', onScenarioStart);
}

register ({
	name : 'scenarioLogic',
	deps : ['Mediator', 'Data'],
	init : init
});
