register ({
	name : 'FOOBAR',
	deps : ['Mediator', 'Data', 'Get'],
	init : init
});

function init(deps) {
	var xhttp = new XMLHttpRequest();
	xhttp.open('GET', '../app/scenario/' + deps.Get.get("scenario"), true);
	xhttp.onreadystatechange = function() {
		if(this.readyState == XMLHttpRequest.DONE) {
			deps.Data.setScenario(deps.Get.get("scenario"));
			deps.Data.setPerson(deps.Get.get("person"));
			deps.Data.setData(JSON.parse(xhttp.responseText));
			deps.Mediator.pushEvent('ScenarioStart');
		}
	};
	xhttp.send();
}
