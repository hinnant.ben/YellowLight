In `src/main/webapp/WEB-INF/web.xml`, modify the directories to point to the
directories containing the scenarios, test data, and images.
`src/main/webapp/WEB-INF/demo/` has examples of what these files should look
like.

To start the server in Mac or Linux, `cd` to the root directory of this
project, and enter `./gradlew jettyRun`. To stop the server, enter `./gradlew
jettyStop`.  To restart the server, enter `./gradlew jettyStop jettyRun`. In
Windows, replace `./gradlew` with `gradlew.bat`.

Once the server is started, go to address to see a list of scenarios from the
scenario CSV file. `localhost:8080/YellowLight`.
